
// 2
var checkTypeArgument = function( arg ){
    switch(typeof arg ){
        case "number":
            console.log("it is number");
            break;
        case "string":
            console.log("it is string");
            break;
        case "boolean":
            console.log("it is boolean");
            break;
        case "function":
            console.log("it is function");
            break;
        case "object":
            switch( Array.isArray( arg ) ){
                case true:
                    console.log( "it is array" );
                    break;
                case false:
                    console.log( "it is object" );
                    break;
            }  
    }
}
console.log( "Solution task №2" );
checkTypeArgument( function(){} ); 


// 3_1
var genRandomNum = function(toNum){
    return Math.floor( Math.random()*toNum );
}

var take = function( func, x){
    var arr = [];
    for( var i = 1; i <= x; i++ ){
        arr.push( func(100) );
    }
    return arr;
}
console.log( "Solution task №3_1" );
console.log( take( genRandomNum, 10 ) );

// 3_2
var mainObject = {
    first: 1,
    second: 2,
    third: 3
}

var countObjectKeys = function( obj ){
    var sum = 0
    for ( var key in obj ){
        sum++
    }
    return sum;
}
console.log( "Solution task №3_2" )
console.log( countObjectKeys( mainObject ) );

// 4
var objectWithDifferntVars = {
    first: 1,
    second: 2,
    third: 3,
    fourth: function(){},
    fifth: {
            fifth_1: 5.1,
            fifth_2: {
                fifth_2_1: 5.21,
                fifth_2_2: 5.22
            }
        }
}

var clone = function cloneRecursion( obj ){
    var newObj = {};
    for ( var key in obj ){
        if ( typeof obj[key] !== "object" ){
            newObj[key] = obj[key]; 
        }
        else{
            newObj[key] = cloneRecursion( obj[key] );
        }
    }
    return newObj;
}
console.log( "Solution task №4" );
console.log( clone( objectWithDifferntVars ));
